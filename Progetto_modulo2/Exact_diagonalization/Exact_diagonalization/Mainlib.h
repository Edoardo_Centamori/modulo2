//
//  Mainlib.h
//  Exact_diagonalization
//
//  Created by Edoardo centamori on 28/10/2019.
//  Copyright © 2019 Edoardo centamori. All rights reserved.
//

#ifndef Mainlib_h
#define Mainlib_h

#include <iostream>
#include <cmath>
#define ARMA_DONT_USE_WRAPPER
#define  ARMA_USE_LAPACK
#include <armadillo>


using namespace std;
using namespace arma;

namespace ML {
    Mat<int> codestate(int n){
        /*
         parameter int n : number of sites
         return Mat<int> (n,2^n) : matrix of states encoded in binary notation
        */
        int N = pow(2,n);
        Mat<int> A(N,n);
        for(int i=0; i<N; i++){
            for( int j=0, i_temp=i; j<n; j++){
                A(i,n-1-j)=i_temp % 2; // Professor uses n-1-j -> j, not sure why!
                i_temp/=2;
            }
        }
        return A;
    }
    
    Mat<double> ham(int n, double h, double g, Mat<int> states, bool PBC){
        //is there any overhead in passing by value and thus copying it?
        // PBC need to be implemented
        int N = pow(2,n);
        Mat<double> A(N,N, fill::zeros);
        //sigma_z * sigma_z coupling
        // notice that it's diagonal thus can affect only terms of A(i,i)!
        for(int i_site=0; i_site<n-1; i_site++){
            for(int i_state=0; i_state<N; i_state++){
                if(states(i_state,i_site)==states(i_state,i_site+1))
                    A(i_state,i_state) -= 1.0;
                else
                    A(i_state,i_state) += 1.0;
            }
        }
        if(PBC){
            for(int i_state=0; i_state<N; i_state++){
                if(states(i_state,n-1)==states(i_state,0))
                    A(i_state,i_state) -= 1.0;
                else
                    A(i_state,i_state) += 1.0;
            }
        }
        //sigma_z
        
        for(int i_site=0; i_site<n; i_site++){
            for(int i_state=0; i_state<N; i_state++){
                if(states(i_state,i_site)==1)
                    A(i_state,i_state) -= h;
                else
                    A(i_state,i_state) += h;
            }
        }
        //sigma_x
        int i_coupled; //determine wich state is coupled to i_state by sigma_x
        for(int i_site=0; i_site<n; i_site++){
            for(int i_state=0; i_state<N; i_state++){
                if(states(i_state,i_site)==1)
                    i_coupled = i_state - pow(2,n-1-i_site);
                    // e.g. (i_site = 0) |100> (4) is coupled with |000> (0 = 4 - 2^(3-1-0))
                else
                    i_coupled = i_state + pow(2,n-1-i_site);
                    // e.g. (i_site = 0) |010> (2) is coupled with |110> (6 = 2 + 2^(3-1-0))
                A(i_coupled, i_state) += g;
            }
        }
        return A;
    }
    
    double magnetization(int n, double h, double g, Mat<int> states, bool PBC){
        int N = pow(2,n);
        mat H=ham(n, h, g, states, PBC);
        vec eigenvalues;
        mat eigenvectors;
        eig_sym(eigenvalues,eigenvectors, H);
        vec ground = eigenvectors.col(0);
        vec mag(N);
        double magz = 0.0;
        for(int i_state = 0; i_state<N; i_state++){
            mag(i_state) = 0.0;
            for(int i_site = 0; i_site<n; i_site++){
                if(states(i_state,i_site)==1) mag(i_state)+= 1.0;
                else                          mag(i_state)-= 1.0;
            }
            magz += abs(mag(i_state))*pow(abs(ground(i_state)),2);
            //ACHTUNG REMOVE ABS FOR STATE WITH G=0
            // using abs(mag(i_state)) instead of mag(i_state) apparently solve
            // the problem of degenerate background but I don't know why!
        }
        return magz/n; // '/n' provide the mean magnetization
    }
}
#endif /* Mainlib_h */

