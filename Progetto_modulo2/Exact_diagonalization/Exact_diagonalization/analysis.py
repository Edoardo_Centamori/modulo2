import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from scipy.optimize import curve_fit


g,m2,m3,m4,m5,m6,m7,m8,m9 = np.loadtxt('/Users/edoardo/Desktop/moving_g.txt',unpack=True)

M = [m2,m3,m4,m5,m6,m7,m8,m9]

fig, ax= plt.subplots()

ax.set_title('Magnetization along z axis')
ax.set_xlabel('g')
ax.set_ylabel('$M_z$')


n = len(M)

colors = cm.jet(np.linspace(0,1,n))

for index,mz in enumerate(M):
    ax.scatter(g,mz,s=0.1, color=colors[index],label='N = '+ str(index+2)) #ACHTUNG h-1 from data
    ax.grid(color='grey', linestyle='-', linewidth=0.25, alpha=0.5)
    
ax.legend()
plt.show()

def renorm(x,beta,C):
    return C*x**beta
    
    
    
    
#Non fitterà mai con un sistema così piccolo, serve sicurmente di prendere una piccola porzione attorno a g=1 a sinistra e perchè fitti serve che M(0)~0! per sistemi piccoli non accade!

#init = [1./8., 1.]
#popt,pcov = curve_fit(renorm, g, m9, init)