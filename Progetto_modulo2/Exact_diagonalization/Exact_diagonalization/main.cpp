#include <iostream>
#include <fstream>
#include <algorithm>
#define ARMA_DONT_USE_WRAPPER
#define  ARMA_USE_LAPACK
#include <armadillo>
#include "Mainlib.h"


using namespace std;
using namespace arma;

int main()
{
    
    int n = 10;  // 12 take order of few seconds
    bool PBC = true;
    Mat<int> A = ML::codestate(n);
    //cout << "A:\n" << A << "\n";
    //Mat<double> H = ML::ham(n, 0.0, 0.1, A, PBC);
    //first parameter is h, second is g
    //cout << "B:\n" << H << "\n";
    /* //useles reading stuff for the moment
    fstream inputstream("/Users/edoardo/Desktop/University/Metodi_numerici/Modulo2/Progetto_modulo2/Exact_diagonalization/Exact_diagonalization/input.txt", ios_base::in); //replace with your path
    //more in general it would be helpful to understand how to make this part portable!
    string line;
    if(inputstream.is_open()){
        while(getline(inputstream, line)){
            //cout << line << endl;
            line.erase(find(line.begin(),line.end(),'#'),line.end()); //ignore comment starting with #
            //It might have problem with a line starting with #, check it out!
            //cout << line << endl;
            istringstream iss(line); // create a stream from a string
            if(find(line.begin(),line.end(),'.') != line.end()){ //handling double
                double a;
                while(iss >> a)
                    cout << a << endl; //It will become an initialization instead of being printed
            }
            else{ //handling int
                int a;
                while(iss >> a)
                    cout << a << endl; //It will become an initialization instead of being printed
            }
        }
    }
    else
        cout << "Input file has not ben opened properly" << endl;
    inputstream.close();
     */
    //vec eigenvalues;
    //mat eigenvectors;
    //eig_sym(eigenvalues,eigenvectors, H);
    //cout << "Eigenvalues are : " << eigenvalues << endl;
    //double expected;
    //expected = 1.0-1.0/sin(M_PI/(2*(2*n+1)));
    double h_l = -1.0, h_r=1.0, g_l=-0.0, g_r=2.0;
    int repetition = 200;
    vec H(repetition), G(repetition);
    for(int i=0; i<repetition; i++){
        H(i) = (h_r-h_l)/(repetition-1)*i+h_l;
        G(i) = (g_r-g_l)/(repetition-1)*i+g_l;
    }
    cout << "H : \n" << H << endl;
    double m = ML::magnetization(n, 0.2, 0.3, A, PBC=true);
    cout << "magnetizazion : " << m << endl;
    
    fstream outputstream("/Users/edoardo/Desktop/University/Metodi_numerici/Modulo2/Progetto_modulo2/Exact_diagonalization/Exact_diagonalization/results.txt", ofstream::out | ofstream::trunc); //replace with your path
    //more in general it would be helpful to understand how to make this part portable!
    outputstream << "# h    #|M_z|"<< endl;
    for(int i=0; i<repetition; i++){
        outputstream << G(i);
        for(int n=2; n<10; n++){
            Mat<int> A = ML::codestate(n);
            double mz = ML::magnetization(n, H(i), 1.0, A, PBC=true);
            outputstream << "    " << mz;
        }
        outputstream << endl;
    }
    outputstream.close();
    return 0;
}
